// _.difference - Creates an array of array values not included in the other given arrays using SameValueZero for equality comparisons. The order and references of result values are determined by the first array.

export default (array1, array2) => array1.filter(el => !array2.includes(el))
