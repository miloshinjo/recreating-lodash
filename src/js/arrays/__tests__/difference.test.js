import difference from '../difference'

const testArray1 = [1, 2, 3, 4, 5]
const testArray2 = [1, 3, 5]

let result

beforeEach(() => {
  result = difference(testArray1, testArray2)
})

test('should return a new array with values not included ', () => {
  expect(result).toEqual([2, 4])
})

test('should not edit the original arrays', () => {
  expect(testArray1.length + testArray2.length).toBe(8)
})
