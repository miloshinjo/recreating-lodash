import drop from '../drop'

const testArray = [1, 2, 3, 4, 5]

let result

beforeEach(() => {
  result = drop(testArray, 3)
})

test('should return chunked array', () => {
  expect(result).toEqual([4, 5])
})

test('should not edit the original array', () => {
  expect(testArray.length).toBe(5)
})
