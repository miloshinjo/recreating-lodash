import chunk from '../chunk'

const testArray = [2, 4, 6, 7, 'john']

let result

beforeEach(() => {
  result = chunk(testArray, 2)
})

test('should return chunked array', () => {
  expect(result).toEqual([[2, 4], [6, 7], ['john']])
})

test('should not edit the original array', () => {
  expect(testArray.length).toBe(5)
})
