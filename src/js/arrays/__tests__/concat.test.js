import concat from '../concat'

export const testArray1 = [2, 4, 6]
export const testArray2 = ['John', 'Andy']

test('should concat 2 arrays into 2', () => {
  const result = concat(testArray1, testArray2)

  expect(result).toEqual([2, 4, 6, 'John', 'Andy'])
})

test('should concat 2 arrays and additional values', () => {
  const result = concat(testArray1, testArray2, 2, [4])

  expect(result).toEqual([2, 4, 6, 'John', 'Andy', 2, [4]])
})

test('should not edit original arrays', () => {
  concat(testArray1, testArray2)

  expect(testArray1.length + testArray2.length).toBe(5)
})
