import compact from '../compact'

const testArray = [NaN, '', 'Jane Smith', 12, {}, false, undefined, null, 0, -1]

test('should remove all falsy values from the array', () => {
  const result = compact(testArray)

  expect(result).toEqual(['Jane Smith', 12, {}, -1])
})
