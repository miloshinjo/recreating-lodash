// _.concat - Creates a new array concatenating array with any additional arrays and/or values.

export default (array1, array2, ...restArgs) => [...array1, ...array2, ...restArgs]
