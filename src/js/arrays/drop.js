// _.drop - Creates a slice of array with n elements dropped from the beginning.
export default (array, n = 1) => array.slice(n)
