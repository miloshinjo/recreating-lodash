import chunk from './chunk'
import compact from './compact'
import concat from './concat'
import difference from './difference'
import drop from './drop'
import { numberArray, numberArray2, stringArray, randomArray } from './__arrays__'

// Usage
console.log('chunk', chunk(randomArray, 2))
console.log('compact', compact(randomArray))
console.log('concat', concat(numberArray, stringArray))
console.log('difference', difference(numberArray, numberArray2))
console.log('drop', drop(numberArray, 4))
