export const randomArray = ['John Smith', NaN, '', 'Jane Smith', 12, { pure: false }, false, 15, 'Adam Smith', undefined, 3, 'Milos', function () {
  console.log('Im a function')
}, null, 0, -1]

export const numberArray = [1, 2, 3, 4, 5, 6, 7]
export const numberArray2 = [2, 4, 6, 7]
export const stringArray = ['John', 'Andy', 'John', 'History', 'Buffs']
